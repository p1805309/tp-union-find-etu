#ifndef LIFAP6_UNIONFIND_HPP_
#define LIFAP6_LABYRINTHE_HPP_

#include <vector>

class UnionFind
{
private:
    std::vector<int> arbre;
public:
    UnionFind(int size);
    ~UnionFind();
    int find(int Arbre);
    bool find(int ArbreA, int ArbreB);
    bool unionArbre(int ArbreA, int ArbreB);
};





#endif
