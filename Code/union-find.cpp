#include "union-find.hpp"




UnionFind::UnionFind(int size)
{
    arbre.resize(size);
    for (int i = 0; i < size; i++)
    {
        arbre[i]=i;
    }
    
}


UnionFind::~UnionFind()
{
}


int UnionFind::find(int Arbre){
    int current = Arbre;
    int racine = arbre[current];
    while (racine != current){
        current = racine;
        racine = arbre[current];
    }
    return racine;
}

bool UnionFind::find(int ArbreA, int ArbreB){
    int RArbreA = find(ArbreA);
    int RArbreB = find(ArbreB);
    return RArbreA == RArbreB;
}

bool UnionFind::unionArbre(int ArbreA, int ArbreB){

    if (find(ArbreA,ArbreB)) return false;
    int RArbreA = find(ArbreA);
    int RArbreB = find(ArbreB);
    arbre[RArbreB] = RArbreA;
    return true;
}